<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <virtualType name="Afterpay\Payment\Gateway\Config\OpenInvoiceSE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_OI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\CampaignSE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\OpenInvoiceSEB2B\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_B2B_OI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign2SE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP2</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign3SE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP3</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\InstallmentSE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_IN</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\FlexSE\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_FX</argument>
        </arguments>
    </virtualType>


    <!-- OPEN INVOICE SE -->


    <virtualType name="OpenInvoiceSEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_OI</argument>
            <argument name="valueHandlerPool" xsi:type="object">OpenInvoiceSEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">OISECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">OISEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">OISEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">OISECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">OISESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">OISERefundCommand</item>
                <item name="void" xsi:type="string">OISEVoidCommand</item>
                <item name="cancel" xsi:type="string">OISEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">OISEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">OISECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OpenInvoiceSEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">OISEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OISECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">OISECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Campaign invoice SE -->


    <virtualType name="CampaignSEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP</argument>
            <argument name="valueHandlerPool" xsi:type="object">CampaignSEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CPSECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CPSEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CPSEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CPSECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CPSESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CPSERefundCommand</item>
                <item name="void" xsi:type="string">CPSEVoidCommand</item>
                <item name="cancel" xsi:type="string">CPSEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPSETransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSEAuthorizeRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="payment_campaign" xsi:type="string">Afterpay\Payment\Gateway\Request\PaymentCampaignDataBuilder</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CPSEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CPSECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CampaignSEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CPSEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPSEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CPSECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- SECOND Campaign invoice SE -->


    <virtualType name="Campaign2SEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP2</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign2SEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP2SECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP2SEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP2SEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP2SECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP2SESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP2SERefundCommand</item>
                <item name="void" xsi:type="string">CP2SEVoidCommand</item>
                <item name="cancel" xsi:type="string">CP2SEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2SETransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP2SEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP2SECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2SE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign2SEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP2SEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2SE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2SEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP2SECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- THIRD Campaign invoice SE -->


    <virtualType name="Campaign3SEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_CP3</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign3SEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP3SECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP3SEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP3SEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP3SECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP3SESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP3SERefundCommand</item>
                <item name="void" xsi:type="string">CP3SEVoidCommand</item>
                <item name="cancel" xsi:type="string">CP3SEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3SETransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP3SEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP3SECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3SETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3SE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign3SEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP3SEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3SE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3SEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP3SECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Fixed instalments SE -->


    <virtualType name="InstallmentSEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_IN</argument>
            <argument name="valueHandlerPool" xsi:type="object">InstallmentSEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">INSECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">INSEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">INSEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">INSECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">INSESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">INSERefundCommand</item>
                <item name="void" xsi:type="string">INSEVoidCommand</item>
                <item name="cancel" xsi:type="string">INSEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INSEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">INSEAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">INSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">INSEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSEAuthorizeRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="payment_installment" xsi:type="string">Afterpay\Payment\Gateway\Request\PaymentInstallmentSSNDataBuilder</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INSECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">INSECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="InstallmentSEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">INSEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INSECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="INSEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">INSECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Flex payment SE -->


    <virtualType name="FlexSEFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_FX</argument>
            <argument name="valueHandlerPool" xsi:type="object">FlexSEValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">FXSECommandPool</argument>
            <argument name="validatorPool" xsi:type="object">FXSEValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSECommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">FXSEAuthorizeCommand</item>
                <item name="capture" xsi:type="string">FXSECaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">FXSESaleCaptureCommand</item>
                <item name="refund" xsi:type="string">FXSERefundCommand</item>
                <item name="void" xsi:type="string">FXSEVoidCommand</item>
                <item name="cancel" xsi:type="string">FXSEVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSEVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSEAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">FXSEAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">FXSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSETransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">FXSEConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSEAuthorizeRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="payment_flex" xsi:type="string">Afterpay\Payment\Gateway\Request\PaymentFlexDataBuilder</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSECaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">FXSECommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSESaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSERefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXSETransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSEConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FlexSEValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">FXSEConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSECountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXSEValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">FXSECountryValidator</item>
            </argument>
        </arguments>
    </virtualType>

    <!-- OPEN INVOICE SE B2B -->

    <virtualType name="OpenInvoiceSEB2BFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_B2B_OI</argument>
            <argument name="valueHandlerPool" xsi:type="object">OpenInvoiceSEB2BValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">OISEB2BCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">OISEB2BValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">OISEB2BAuthorizeCommand</item>
                <item name="capture" xsi:type="string">OISEB2BCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">OISEB2BSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">OISEB2BRefundCommand</item>
                <item name="void" xsi:type="string">OISEB2BVoidCommand</item>
                <item name="cancel" xsi:type="string">OISEB2BVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISEB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">OISEB2BRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">OISEB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">OISEB2BConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">OISEB2BCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISEB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OISEB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceSEB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OpenInvoiceSEB2BValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">OISEB2BConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceSEB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">OISEB2BCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OISEB2BRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="customer_business" xsi:type="string">Afterpay\Payment\Gateway\Request\BusinessCustomerDataRestBuilder</item>
            </argument>
        </arguments>
    </virtualType>
</config>
