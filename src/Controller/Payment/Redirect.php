<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Url\Decoder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\OrderRepository;

class Redirect extends Action
{
    public const REQUEST_PARAM_TOKEN = 'token';

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Decoder
     */
    private $decoder;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @param Context         $context
     * @param ResultFactory   $resultFactory
     * @param Session         $session
     * @param Decoder         $decoder
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        Context $context,
        ResultFactory $resultFactory,
        Session $session,
        Decoder $decoder,
        OrderRepository $orderRepository
    ) {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
        $this->session = $session;
        $this->decoder = $decoder;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $order = $this->getOrder();
        if (!$order) {
            return $result->setPath('/');
        }
        if ($redirectUrl = $order->getAfterpayScaRedirectUrl()) {
            return $result->setUrl($this->decoder->decode($redirectUrl));
        }

        return $result->setPath('checkout/onepage/success');
    }

    /**
     * @return OrderInterface|null
     */
    protected function getOrder(): ?OrderInterface
    {
        $orderId = $this->session->getLastOrderId();
        try {
            return $this->orderRepository->get($orderId);
        } catch (InputException | NoSuchEntityException $e) {
            return null;
        }
    }
}
