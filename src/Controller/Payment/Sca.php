<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Controller\Payment;

use Afterpay\Payment\Model\ScaHandler;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Afterpay\Payment\Model\Order\Email\Sender\AfterpayOrderSender as OrderSender;
use Magento\Checkout\Model\Session;
use Magento\Framework\Event\ManagerInterface;

class Sca extends Action
{
    public const REQUEST_PARAM_TOKEN = 'token';

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ScaHandler
     */
    private $scaHandler;

    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @param Context                  $context
     * @param ResultFactory            $resultFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderSender              $orderSender
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder
     * @param ScaHandler               $scaHandler
     * @param Session                  $checkoutSession
     */
    public function __construct(
        Context $context,
        ResultFactory $resultFactory,
        OrderRepositoryInterface $orderRepository,
        OrderSender $orderSender,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ScaHandler $scaHandler,
        Session $checkoutSession,
        ManagerInterface $eventManager
    ) {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
        $this->orderRepository = $orderRepository;
        $this->orderSender = $orderSender;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->scaHandler = $scaHandler;
        $this->checkoutSession = $checkoutSession;
        $this->messageManager = $context->getMessageManager();
        $this->eventManager = $eventManager;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRaw */
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $order = $this->getOrder();
        if (!$order) {
            return $resultRaw->setPath('/');
        }
        $orderStatus = $this->handleOrder($order);
        if ($orderStatus) {
            $this->eventManager->dispatch('afterpay_order_capture', ['order' => $order]);
            return $resultRaw->setPath('checkout/onepage/success');
        }
        $this->messageManager->addErrorMessage(
            'There was a issue processing you Strong Customer Authentication request'
        );
        return $resultRaw->setPath('checkout/cart');
    }

    protected function getOrder(): ?OrderInterface
    {
        $token = $this->getRequest()->getParam(self::REQUEST_PARAM_TOKEN);
        try {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('afterpay_sca_hash', $token)
                ->setPageSize(1)
                ->setCurrentPage(1)
                ->create();
            $results = $this->orderRepository->getList($searchCriteria);
            return current($results->getItems());
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    protected function handleOrder(OrderInterface $order): bool
    {
        $orderStatusResponse = $this->scaHandler->getOrderStatus($order);
        if ($orderStatusResponse) {
            $this->updateOrderInformationSuccess($order);
            $this->sendOrderConfirmationEmail($order);
        } else {
            $this->updateOrderInformationFailed($order);
        }

        return $orderStatusResponse;
    }

    /**
     * @param OrderInterface $order
     *
     * @return void
     */
    protected function updateOrderInformationSuccess(OrderInterface $order): void
    {
        $order->setStatus(Order::STATE_PROCESSING)
              ->setState(Order::STATE_PROCESSING);
        $order->addCommentToStatusHistory(
            'Strong Customer Authentication has been successfully completed by the customer'
        );
        $this->orderRepository->save($order);
    }

    /**
     * @param OrderInterface $order
     *
     * @return void
     */
    protected function updateOrderInformationFailed(OrderInterface $order): void
    {
        $order->setStatus(ScaHandler::ORDER_STATUS_SCA_FAILED_CODE)
              ->setState(Order::STATE_PAYMENT_REVIEW);
        $order->addCommentToStatusHistory(
            'Strong Customer Authentication hasn\'t been successfully completed by the customer'
        );
        $this->orderRepository->save($order);
        $this->checkoutSession->restoreQuote();
    }

    /**
     * @param OrderInterface $order
     *
     * @return void
     */
    protected function sendOrderConfirmationEmail($order)
    {
        $this->orderSender->send($order, true);
    }
}
