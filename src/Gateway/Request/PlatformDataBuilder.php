<?php

/**
 * @category  Scandiweb
 * @package   Afterpay\Payment\Gateway\Request
 * @author    austris <info@scandiweb.com>
 * @copyright Copyright (c) 2021 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Block\Adminhtml\System\Config\Form\Field\Version;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\ResourceInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Store\Model\StoreManagerInterface;

class PlatformDataBuilder implements BuilderInterface
{
    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ResourceInterface
     */
    private $moduleResource;

    public function __construct(
        ProductMetadataInterface $productMetadata,
        ResourceInterface $moduleResource,
        StoreManagerInterface $storeManager
    ) {
        $this->productMetadata = $productMetadata;
        $this->storeManager = $storeManager;
        $this->moduleResource = $moduleResource;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        return [
            'additionalData' => [
                'pluginProvider' => 'Arvato',
                'pluginVersion' => $this->moduleResource->getDbVersion(Version::MODULE_NAME),
                'shopUrl' => $this->storeManager->getStore()->getBaseUrl(),
                'shopPlatform' => 'Magento',
                'shopPlatformVersion' => $this->productMetadata->getVersion()
            ]
        ];
    }
}
