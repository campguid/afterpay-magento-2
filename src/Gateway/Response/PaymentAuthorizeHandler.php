<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Response;

use Afterpay\Payment\Model\Config\Source\ReplaceAddressType;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;

class PaymentAuthorizeHandler implements HandlerInterface
{
    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        // TODO create custom subjectReader to not have typehint everything
        $this->subjectReader = $subjectReader;
    }

    /**
     * @param array $handlingSubject
     * @param array $response
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader::readPayment($handlingSubject);
        /** @var Payment $orderPayment */
        $orderPayment = $paymentDO->getPayment();
        /* @var $order Order */
        $order = $orderPayment->getOrder();

        if ($paymentDO->getPayment() instanceof Payment) {
            $this->setTransaction(
                $orderPayment,
                $response['object']->reservationId
            );
            $this->setNewAddress($orderPayment, $response);
            // it is authorized only yet, so naming may be confusing
            $order->setAfterpayCaptured(1);
        }
    }

    /**
     * @param Payment $orderPayment
     * @param string  $transaction
     *
     * @return void
     */
    protected function setTransaction(Payment $orderPayment, $transaction)
    {
        $orderPayment->setTransactionId($transaction);
        $orderPayment->setIsTransactionClosed(false);
        $orderPayment->setShouldCloseParentTransaction(false);
    }

    /**
     * @param Payment $orderPayment
     * @param array   $response
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setNewAddress(Payment $orderPayment, array $response)
    {
        $methodInstance = $orderPayment->getMethodInstance();
        $responseObj = $response['object'];
        $order = $orderPayment->getOrder();
        if ((int) $methodInstance->getConfigData('replace_address')
            === ReplaceAddressType::REPLACE_ADDRESS_TYPE_NOTHING_CODE) {
            return;
        }

        if (array_key_exists('customer', $responseObj)) {
            if (array_key_exists('addressList', $responseObj->customer)) {
                $suggestedAddress = $responseObj->customer->addressList[0];
                switch ((int) $methodInstance->getConfigData('replace_address')) {
                    case ReplaceAddressType::REPLACE_ADDRESS_TYPE_BILLING_CODE:
                        $this->setSuggestedBillingAddress($suggestedAddress, $order);
                        break;
                    case ReplaceAddressType::REPLACE_ADDRESS_TYPE_SHIPPING_CODE:
                        $this->setSuggestedShippingAddress($suggestedAddress, $order);
                        break;
                    case ReplaceAddressType::REPLACE_ADDRESS_TYPE_BOTH_CODE:
                        $this->setSuggestedShippingAndBillingAddress($suggestedAddress, $order);
                        break;
                }
            }
        }
    }

    /**
     * @param \stdClass $suggestedAddress
     * @param Order     $order
     */
    protected function setSuggestedShippingAddress(\stdClass $suggestedAddress, Order $order)
    {
        $address = $this->parseSuggestedShippingAddress($suggestedAddress);
        $fullStreet = sprintf(
            '%1$s %2$s %3$s',
            $address['streetName'],
            $address['streetNumber'],
            $address['streetAdditionalNumber']
        );
        $shippingAddress = $order->getShippingAddress();
        $shippingAddress->setStreet($fullStreet)
                        ->setPostcode($suggestedAddress->postalCode)
                        ->setCity($suggestedAddress->postalPlace)
                        ->setCountryId($suggestedAddress->countryCode);
    }

    /**
     * @param \stdClass $suggestedAddress
     * @param Order     $order
     */
    protected function setSuggestedBillingAddress(\stdClass $suggestedAddress, Order $order)
    {
        $address = $this->parseSuggestedShippingAddress($suggestedAddress);
        $fullStreet = sprintf(
            '%1$s %2$s %3$s',
            $address['streetName'],
            $address['streetNumber'],
            $address['streetAdditionalNumber']
        );
        $billingAddress = $order->getBillingAddress();
        $billingAddress->setStreet($fullStreet)
                       ->setPostcode($suggestedAddress->postalCode)
                       ->setCity($suggestedAddress->postalPlace)
                       ->setCountryId($suggestedAddress->countryCode);
    }

    /**
     * @param \stdClass $suggestedAddress
     * @param Order     $order
     */
    protected function setSuggestedShippingAndBillingAddress(\stdClass $suggestedAddress, Order $order)
    {
        $this->setSuggestedBillingAddress($suggestedAddress, $order);
        $this->setSuggestedShippingAddress($suggestedAddress, $order);
    }

    /**
     * @param \stdClass $suggestedAddress
     *
     * @return array
     */
    protected function parseSuggestedShippingAddress(\stdClass $suggestedAddress): array
    {
        $address = [];
        $address['streetName'] = $suggestedAddress->street ?? '';
        $address['streetNumber'] = $suggestedAddress->streetNumber ?? '';
        $address['streetAdditionalNumber'] = $suggestedAddress->streetNumberAdditional ?? '';
        return $address;
    }
}
