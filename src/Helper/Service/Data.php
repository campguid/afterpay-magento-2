<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Helper\Service;

use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * Invoice not capture
     */
    const INVOICE_NOT_CAPTURE_ACTIVE = 1;

    /**
     * Order statuses
     */
    const ORDER_STATUS_PENDING = 'pending';
    const ORDER_STATUS_PROCESSING = 'processing';

    /**
     * Label that gets shown in checkout payment section
     */
    const TEST_MODE_LABEL = 'Test mode, orders will not be fulfilled.';

    /**
     * XML Fields for configuration constants
     */
    const XML_FIELD_TEST_MODE_ACTIVE = 'testmode';
    const XML_FIELD_TEST_MODE_MERCHANT_ID = 'testmode_merchant_id';
    const XML_FIELD_TEST_MODE_PORTFOLIO_ID = 'testmode_portfolio_id';
    const XML_FIELD_TEST_MODE_PASSWORD = 'testmode_password';
    const XML_FIELD_TEST_MODE_PUSH_PASSWORD = 'testmode_push_password';
    const XML_FIELD_PROD_MODE_MERCHANT_ID = 'production_merchant_id';
    const XML_FIELD_PROD_MODE_PORTFOLIO_ID = 'production_portfolio_id';
    const XML_FIELD_PROD_MODE_API_KEY = 'production_api_key';
    const XML_FIELD_TEST_MODE_API_KEY = 'testmode_api_key';
    const XML_FIELD_PROD_MODE_PASSWORD = 'production_password';
    const XML_FIELD_PROD_MODE_PUSH_PASSWORD = 'production_push_password';
    const XML_FIELD_REFUND_ENABLED = 'payment/afterpay_refund/active';
    const XML_FIELD_VOID_ENABLED = 'payment/afterpay_void/active';
    const XML_FIELD_HAS_REST = 'has_rest';

    /**
     * Afterpay SVG icon URL
     */
    const AFTERPAY_SVG_ICON = 'https://cdn.myafterpay.com/logo/AfterPay_logo_checkout.svg';

    /**
     * All payment method const names
    */

    // NL
    const AFTERPAY_NL_DI = 'afterpay_nl_digital_invoice';
    const AFTERPAY_NL_DI_EXTRA = 'afterpay_nl_digital_invoice_extra';
    const AFTERPAY_NL_DD = 'afterpay_nl_direct_debit';
    const AFTERPAY_NL_B2B = 'afterpay_nl_business_2_business';
    const AFTERPAY_NL_REST_DI = 'afterpay_nl_rest_invoice';
    const AFTERPAY_NL_REST_EXTRA_DI = 'afterpay_nl_rest_invoice_extra';
    const AFTERPAY_NL_REST_DD = 'afterpay_nl_rest_direct_debit';
    const AFTERPAY_NL_REST_B2B = 'afterpay_nl_rest_b2b';

    // DE
    const AFTERPAY_DE_DI = 'afterpay_de_invoice';
    const AFTERPAY_DE_DD = 'afterpay_de_direct_debit';
    const AFTERPAY_DE_IN = 'afterpay_de_installment';
    const AFTERPAY_DE_B2B = 'afterpay_de_b2b';

    // BE
    const AFTERPAY_BE_DI = 'afterpay_be_digital_invoice';
    const AFTERPAY_BE_DI_EXTRA = 'afterpay_be_digital_invoice_extra';
    const AFTERPAY_BE_REST_DI = 'afterpay_be_rest_invoice';
    const AFTERPAY_BE_REST_EXTRA_DI = 'afterpay_be_rest_invoice_extra';
    const AFTERPAY_BE_REST_DD = 'afterpay_be_rest_direct_debit';
    const AFTERPAY_BE_REST_B2B = 'afterpay_be_rest_b2b';

    // AT
    const AFTERPAY_AT_OI = 'afterpay_at_open_invoice';
    const AFTERPAY_AT_DD = 'afterpay_at_direct_debit';
    const AFTERPAY_AT_IN = 'afterpay_at_installment';

    // CH
    const AFTERPAY_CH_OI = 'afterpay_ch_open_invoice';

    // SE
    const AFTERPAY_SE_OI = 'afterpay_se_open_invoice';
    const AFTERPAY_SE_CP = 'afterpay_se_campaign';
    const AFTERPAY_SE_CP2 = 'afterpay_se_campaign2';
    const AFTERPAY_SE_CP3 = 'afterpay_se_campaign3';
    const AFTERPAY_SE_IN = 'afterpay_se_installment';
    const AFTERPAY_SE_FX = 'afterpay_se_flex';
    const AFTERPAY_SE_B2B_OI = 'afterpay_se_b2b_open_invoice';

    // FI
    const AFTERPAY_FI_OI = 'afterpay_fi_open_invoice';
    const AFTERPAY_FI_CP = 'afterpay_fi_campaign';
    const AFTERPAY_FI_CP2 = 'afterpay_fi_campaign2';
    const AFTERPAY_FI_CP3 = 'afterpay_fi_campaign3';
    const AFTERPAY_FI_IN = 'afterpay_fi_installment';
    const AFTERPAY_FI_FX = 'afterpay_fi_flex';
    const AFTERPAY_FI_B2B_OI = 'afterpay_fi_b2b_open_invoice';

    // NO
    const AFTERPAY_NO_OI = 'afterpay_no_open_invoice';
    const AFTERPAY_NO_CP = 'afterpay_no_campaign';
    const AFTERPAY_NO_CP2 = 'afterpay_no_campaign2';
    const AFTERPAY_NO_CP3 = 'afterpay_no_campaign3';
    const AFTERPAY_NO_IN = 'afterpay_no_installment';
    const AFTERPAY_NO_FX = 'afterpay_no_flex';
    const AFTERPAY_NO_B2B_OI = 'afterpay_no_b2b_open_invoice';

    // DK
    const AFTERPAY_DK_DI = 'afterpay_dk_digital_invoice';
    const AFTERPAY_DK_B2B_DI = 'afterpay_dk_b2b_digital_invoice';
    const AFTERPAY_DK_CP = 'afterpay_dk_campaign';
    const AFTERPAY_DK_CP2 = 'afterpay_dk_campaign2';
    const AFTERPAY_DK_CP3 = 'afterpay_dk_campaign3';
    const AFTERPAY_DK_FX = 'afterpay_dk_flex';
    const AFTERPAY_DK_IN = 'afterpay_dk_installment';

    /**
     * Iso languages for payment methods types
     *
     * @var array
     */
    protected $isoLanguages = [
        'afterpay_nl_digital_invoice' => 'NL',
        'afterpay_nl_digital_invoice_extra' => 'NL',
        'afterpay_nl_direct_debit' => 'NL',
        'afterpay_nl_business_2_business' => 'NL',
        'afterpay_nl_rest_invoice' => 'NL',
        'afterpay_nl_rest_invoice_extra' => 'NL',
        'afterpay_nl_rest_direct_debit' => 'NL',
        'afterpay_nl_rest_b2b' => 'NL',
        'afterpay_be_digital_invoice' => 'NL',
        'afterpay_be_digital_invoice_extra' => 'NL',
        'afterpay_be_rest_invoice' => 'NL',
        'afterpay_be_rest_invoice_extra' => 'NL',
        'afterpay_be_rest_direct_debit' => 'NL',
        'afterpay_be_rest_b2b' => 'NL',
        'afterpay_de_invoice' => 'DE',
        'afterpay_de_direct_debit' => 'DE',
        'afterpay_de_b2b' => 'DE',
        'afterpay_at_open_invoice' => 'DE',
        'afterpay_at_direct_debit' => 'DE',
        'afterpay_at_installment' => 'DE',
        'afterpay_de_installment' => 'DE',
        'afterpay_ch_open_invoice' => 'DE',
        'afterpay_se_open_invoice' => 'SE',
        'afterpay_se_campaign' => 'SE',
        'afterpay_se_campaign2' => 'SE',
        'afterpay_se_campaign3' => 'SE',
        'afterpay_se_installment' => 'SE',
        'afterpay_se_b2b_open_invoice' => 'SE',
        'afterpay_se_flex' => 'SE',
        'afterpay_fi_open_invoice' => 'FI',
        'afterpay_fi_b2b_open_invoice' => 'FI',
        'afterpay_fi_campaign' => 'SE',
        'afterpay_fi_campaign2' => 'SE',
        'afterpay_fi_campaign3' => 'SE',
        'afterpay_fi_installment' => 'FI',
        'afterpay_fi_flex' => 'FI',
        'afterpay_no_open_invoice' => 'NO',
        'afterpay_no_b2b_open_invoice' => 'NO',
        'afterpay_no_campaign' => 'SE',
        'afterpay_no_campaign2' => 'SE',
        'afterpay_no_campaign3' => 'SE',
        'afterpay_no_installment' => 'NO',
        'afterpay_no_flex' => 'NO',
        'afterpay_dk_digital_invoice' => 'DK',
        'afterpay_dk_b2b_digital_invoice' => 'DK',
        'afterpay_dk_campaign' => 'SE',
        'afterpay_dk_campaign2' => 'SE',
        'afterpay_dk_campaign3' => 'SE',
        'afterpay_dk_flex' => 'DK',
        'afterpay_dk_installment' => 'DK'
    ];

    /**
     * @var string[]
     */
    public static $allowedMethods = [
        self::AFTERPAY_AT_IN,
        self::AFTERPAY_DE_IN,
        self::AFTERPAY_FI_IN,
        self::AFTERPAY_NO_IN,
        self::AFTERPAY_SE_IN,
        self::AFTERPAY_DK_IN,
        self::AFTERPAY_DK_FX,
        self::AFTERPAY_FI_FX,
        self::AFTERPAY_NO_FX,
        self::AFTERPAY_SE_FX,
        self::AFTERPAY_SE_CP,
        self::AFTERPAY_SE_CP2,
        self::AFTERPAY_SE_CP3,
        self::AFTERPAY_NO_CP,
        self::AFTERPAY_NO_CP2,
        self::AFTERPAY_NO_CP3,
        self::AFTERPAY_FI_CP,
        self::AFTERPAY_FI_CP2,
        self::AFTERPAY_FI_CP3,
        self::AFTERPAY_DK_CP,
        self::AFTERPAY_DK_CP2,
        self::AFTERPAY_DK_CP3,
    ];

    /**
     * @var string[]
     */
    public static $allowedInstalment = [
        self::AFTERPAY_AT_IN,
        self::AFTERPAY_DE_IN,
        self::AFTERPAY_DK_IN,
        self::AFTERPAY_FI_IN,
        self::AFTERPAY_NO_IN,
        self::AFTERPAY_SE_IN
    ];

    /**
     * @var string[]
     */
    public static $allowedFlex = [
        self::AFTERPAY_DK_FX,
        self::AFTERPAY_FI_FX,
        self::AFTERPAY_NO_FX,
        self::AFTERPAY_SE_FX,
    ];

    /**
     * @var string[]
     */
    public static $allowedCampaigns = [
        self::AFTERPAY_SE_CP,
        self::AFTERPAY_SE_CP2,
        self::AFTERPAY_SE_CP3,
        self::AFTERPAY_FI_CP,
        self::AFTERPAY_FI_CP2,
        self::AFTERPAY_FI_CP3,
        self::AFTERPAY_NO_CP,
        self::AFTERPAY_NO_CP2,
        self::AFTERPAY_NO_CP3,
        self::AFTERPAY_DK_CP,
        self::AFTERPAY_DK_CP2,
        self::AFTERPAY_DK_CP3,
    ];

    /**
     * Nordic payment list
     *
     * @var string[]
     */
    private static $sallowRestrictDifferentBilling = [
        self::AFTERPAY_DK_FX,
        self::AFTERPAY_DK_DI,
        self::AFTERPAY_DK_IN,
        self::AFTERPAY_FI_FX,
        self::AFTERPAY_FI_IN,
        self::AFTERPAY_FI_OI,
        self::AFTERPAY_NO_FX,
        self::AFTERPAY_NO_IN,
        self::AFTERPAY_NO_B2B_OI,
        self::AFTERPAY_SE_B2B_OI,
        self::AFTERPAY_FI_B2B_OI,
        self::AFTERPAY_DK_B2B_DI,
        self::AFTERPAY_NO_OI,
        self::AFTERPAY_SE_FX,
        self::AFTERPAY_SE_IN,
        self::AFTERPAY_SE_OI,
        self::AFTERPAY_SE_CP,
        self::AFTERPAY_SE_CP2,
        self::AFTERPAY_SE_CP3,
        self::AFTERPAY_FI_CP,
        self::AFTERPAY_FI_CP2,
        self::AFTERPAY_FI_CP3,
        self::AFTERPAY_NO_CP,
        self::AFTERPAY_NO_CP2,
        self::AFTERPAY_NO_CP3,
        self::AFTERPAY_DK_CP,
        self::AFTERPAY_DK_CP2,
        self::AFTERPAY_DK_CP3,
        self::AFTERPAY_DE_B2B,
        self::AFTERPAY_DE_DD,
        self::AFTERPAY_DE_DI,
        self::AFTERPAY_DE_IN,
        self::AFTERPAY_AT_DD,
        self::AFTERPAY_AT_IN,
        self::AFTERPAY_AT_OI,
        self::AFTERPAY_CH_OI,
        self::AFTERPAY_NL_REST_B2B,
        self::AFTERPAY_NL_REST_DD,
        self::AFTERPAY_NL_REST_DI,
        self::AFTERPAY_NL_REST_EXTRA_DI,
        self::AFTERPAY_BE_REST_B2B,
        self::AFTERPAY_BE_REST_DD,
        self::AFTERPAY_BE_REST_DI,
        self::AFTERPAY_BE_REST_EXTRA_DI,
    ];

    /**
     * DACH payment list
     *
     * @var string[]
     */
    public static $dachPaymentMethods = [
        self::AFTERPAY_AT_DD,
        self::AFTERPAY_AT_IN,
        self::AFTERPAY_AT_OI,
        self::AFTERPAY_DE_B2B,
        self::AFTERPAY_DE_DD,
        self::AFTERPAY_DE_DI,
        self::AFTERPAY_DE_IN,
        self::AFTERPAY_CH_OI,
    ];

    /**
     * @var string[]
     */
    public static $flexAllowedInfoList = [
        self::AFTERPAY_FI_FX,
        self::AFTERPAY_SE_FX,
        self::AFTERPAY_NO_FX,
        self::AFTERPAY_DK_FX
    ];

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Format date
     *
     * @var DateTime
     */
    protected $dateTime;

    /**
     * Decryptor
     *
     * @var Encrypted
     */
    protected $encrypted;

    /**
     * @var Resolver
     */
    protected $resolver;

    public $orderLineHelperLang = [
        'en' => [
            'discount' => [
                'name' => 'Discount',
                'key' => 'DISCOUNT'
            ],
            'shipping' => [
                'name' => 'Shipping cost',
                'key' => 'SHIPPINGCOST'
            ],
            'fee' => [
                'name' => 'Payment cost',
                'key' => 'PAYMENTCOST'
            ],
            'storecredit' => [
                'name' => 'Store credits',
                'key' => 'STORECREDITS'
            ],
            'giftcard' => [
                'name' => 'Gift card',
                'key' => 'GIFTCARD'
            ],
            'reward' => [
                'name' => 'Reward Points',
                'key' => 'REWARD'
            ],
        ],
        'de' => [
            'discount' => [
                'name' => 'Rabatt',
                'key' => 'RABATT'
            ],
            'shipping' => [
                'name' => 'Versandkosten',
                'key' => 'VERSAND'
            ],
            'fee' => [
                'name' => 'Zahlungsgebühr',
                'key' => 'ZAHLUNG'
            ],
            'storecredit' => [
                'name' => 'Gutschrift',
                'key' => 'GUTSCHRIFT'
            ],
            'giftcard' => [
                'name' => 'Geschenkgutschein',
                'key' => 'GESCHENK'
            ],
            'reward' => [
                'name' => 'Prämienpunkte',
                'key' => 'PUNKTE'
            ]
        ],
        'nl' => [
            'discount' => [
                'name' => 'Korting',
                'key' => 'KORTING'
            ],
            'shipping' => [
                'name' => 'Verzendkosten',
                'key' => 'VERZENDING'
            ],
            'fee' => [
                'name' => 'Betaalkosten',
                'key' => 'BETALING'
            ],
            'storecredit' => [
                'name' => 'Winkeltegoed',
                'key' => 'TEGOED'
            ],
            'giftcard' => [
                'name' => 'Cadeaukaart',
                'key' => 'CADEAU'
            ],
            'reward' => [
                'name' => 'Cadeaukaart',
                'key' => 'SPAARPUNT'
            ]
        ],
        'no' => [
            'discount' => [
                'name' => 'Rabatt',
                'key' => 'RABATT'
            ],
            'shipping' => [
                'name' => 'Fraktkostnad',
                'key' => 'FRAKTKOSTNAD'
            ],
            'fee' => [
                'name' => 'Fakturagebyr',
                'key' => 'FAKTURAGEBYR'
            ],
            'storecredit' => [
                'name' => 'Kredittbeløp',
                'key' => 'KREDITT'
            ],
            'giftcard' => [
                'name' => 'Gavekort',
                'key' => 'GAVEKORT'
            ],
            'reward' => [
                'name' => 'Bonuspoeng',
                'key' => 'BONUS'
            ]
        ],
        'se' => [
            'discount' => [
                'name' => 'Rabatt',
                'key' => 'RABATT'
            ],
            'shipping' => [
                'name' => 'Fraktkostnad',
                'key' => 'FRAKTKOSTNAD'
            ],
            'fee' => [
                'name' => 'Fakturaavgift',
                'key' => 'FAKTURAAVGIFT'
            ],
            'storecredit' => [
                'name' => 'Tillgodobelopp',
                'key' => 'TILLGODO'
            ],
            'giftcard' => [
                'name' => 'Presentkort',
                'key' => 'PRESENTKORT'
            ],
            'reward' => [
                'name' => 'Bonuspoäng',
                'key' => 'BONUS'
            ]
        ],
        'dk' => [
            'discount' => [
                'name' => 'Rabat',
                'key' => 'RABAT'
            ],
            'shipping' => [
                'name' => 'Forsendelsesomkostninger',
                'key' => 'FORSENDELSE'
            ],
            'fee' => [
                'name' => 'Fakturagebyr',
                'key' => 'FAKTURAGEBYR'
            ],
            'storecredit' => [
                'name' => 'Kreditbeløb',
                'key' => 'KREDITT'
            ],
            'giftcard' => [
                'name' => 'Gavekort',
                'key' => 'GAVEKORT'
            ],
            'reward' => [
                'name' => 'Bonuspoint',
                'key' => 'BONUS'
            ]
        ],
        'fi' => [
            'discount' => [
                'name' => 'Alennus',
                'key' => 'ALENNUS'
            ],
            'shipping' => [
                'name' => 'Toimituskulu',
                'key' => 'TOIMITUSKULU'
            ],
            'fee' => [
                'name' => 'Laskutuslisä',
                'key' => 'LASKUTUSLISÄ'
            ],
            'storecredit' => [
                'name' => 'Tilisaldo',
                'key' => 'TILISALDO'
            ],
            'giftcard' => [
                'name' => 'Lahjakortti',
                'key' => 'LAHJAKORTTI'
            ],
            'reward' => [
                'name' => 'Bonuspisteet',
                'key' => 'BONUS'
            ]
        ]
    ];

    /**
     * @param Context $context
     * @param DateTime $dateTime
     * @param Encrypted $encrypted
     * @param Resolver $resolver
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        Encrypted $encrypted,
        Resolver $resolver,
        StoreManagerInterface $storeManager
    ) {
        $this->dateTime = $dateTime;
        $this->encrypted = $encrypted;
        $this->resolver = $resolver;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Get customer initials
     *
     * @param string $firstName first name
     *
     * @return string
     */
    public function getInitials($firstName): string
    {
        return substr($firstName, 0, 9);
    }

    /**
     * Format date of birth for request
     *
     * @param string $dob date of birth
     *
     * @return string|null
     * @throws \Exception
     */
    public function formatDob($dob)
    {
        if ($dob) {
            return (string) str_replace(
                ' ',
                'T',
                $this->dateTime->formatDate(new \DateTime($dob), true)
            );
        }

        return null;
    }

    /**
     * Get ISO language depending on payment method
     *
     * @param string $paymentMethod payment method code
     *
     * @return string
     */
    public function getIsoLanguage($paymentMethod): string
    {
        return $this->isoLanguages[$paymentMethod];
    }

    /**
     * @param $data
     * @param null $key
     *
     * @return null|string
     */
    public function readAdditionalInfo($data, $key = null)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    public function isAfterpayOrder($order)
    {
        $payment = $order->getPayment();
        if ($payment) {
            return strpos($payment->getMethod(), 'afterpay') === 0;
        }

        return false;
    }

    /**
     * @param $config_path
     * @param null $scopeCode
     *
     * @return mixed
     */
    public function getStoreConfig($config_path, $scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * Split address
     *
     * @param $address
     *
     * @return array
     * @throws LocalizedException
     */
    public function getSplitStreet($address)
    {
        $address = \is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname' => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.*?)([0-9]+)(.*)/s', $address, $matches)) {
            if ('' === $matches[1]) {
                // Number at beginning
                $ret['housenumber'] = trim($matches[2]);
                $ret['streetname'] = trim($matches[3]);
            } else {
                // Number at end
                $ret['streetname'] = trim($matches[1]);
                $ret['housenumber'] = trim($matches[2]);
                $ret['houseNumberAddition'] = trim($matches[3]);
            }
        } else {
            $message = 'The house number addition of the shipping address is missing.
            Please check your shipping details or contact our customer service.';
            throw new LocalizedException(__($message));
        }

        return $ret;
    }

    /**
     * @param int $connectionType
     * @param bool $alt
     *
     * @return string
     */
    public function getConnectionType(int $connectionType, bool $alt = false): string
    {
        $connectionTypeMapped = ['production', 'testmode', 'sandbox'];
        if ($alt) {
            $connectionTypeMapped = ['live', 'test', 'sandbox'];
        }
        return $connectionTypeMapped[$connectionType];
    }

    /**
     * @param $paymentMethod string
     *
     * @param null $scopeCode
     *
     * @return mixed
     */
    public function getConfiguration($paymentMethod, $scopeCode = null)
    {
        $connectionType = (int) $this->getStoreConfig(
            sprintf('payment/%s/testmode', $paymentMethod),
            $scopeCode
        );
        $result['modus'] = $this->getConnectionType($connectionType);
        $result['mode'] = $this->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->getStoreConfig(
            sprintf('payment/%s/%s_api_key', $paymentMethod, $result['modus']),
            $scopeCode
        );
        return $result;
    }

    /**
     * @return string
     */
    public function getCurrentLocaleNormalized(): string
    {
        switch ($this->resolver->getLocale()) {
            case 'nl_NL':
            case 'nl_BE':
                $languageCode = 'nl';
                break;
            case 'fr_BE':
                $languageCode = 'fr';
                break;
            case 'nb_NO':
                $languageCode = 'no';
                break;
            case 'sv_SE':
                $languageCode = 'se';
                break;
            case 'fi_FI':
                $languageCode = 'fi';
                break;
            case 'da_DK':
                $languageCode = 'dk';
                break;
            default:
                $languageCode = 'en';
                break;
        }

        return $languageCode;
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getDisablePaymentBillingStepList(): array
    {
        $list = [];
        foreach (self::$sallowRestrictDifferentBilling as $nordicPaymentMethod) {
            if ($this->getValue($nordicPaymentMethod, $this->storeManager->getStore()->getId())) {
                $list[] = sprintf('%s-form', $nordicPaymentMethod);
            }
        }

        return $list;
    }

    /**
     * @param $methodCode
     * @param int|null $storeId
     *
     * @return string
     */
    private function getValue($methodCode, $storeId = null)
    {
        if ($storeId !== null) {
            return $this->scopeConfig->getValue(
                sprintf('payment/%s/disable_billing_step', $methodCode),
                ScopeInterface::SCOPE_STORES,
                $storeId
            );
        }

        return $this->scopeConfig->getValue(sprintf('payment/%s/disable_billing_step', $methodCode));
    }

    /**
     * @param string $methodCode
     *
     * @return string
     */
    public function getMethodCodeNormalizedLanguage(string $methodCode): string
    {
        $languageCodeTemp = $this->isoLanguages[$methodCode] ?? 'EN';
        switch ($languageCodeTemp) {
            case 'NL':
                $languageCode = 'nl';
                break;
            case 'DE':
                $languageCode = 'de';
                break;
            case 'NO':
                $languageCode = 'no';
                break;
            case 'SE':
                $languageCode = 'se';
                break;
            case 'FI':
                $languageCode = 'fi';
                break;
            case 'DK':
                $languageCode = 'dk';
                break;
            default:
                $languageCode = 'en';
                break;
        }

        return $languageCode;
    }

    /**
     * @param string $methodCode
     *
     * @return array
     */
    public function getLineItemLangNormalized(string $methodCode): array
    {
        $langCode = $this->getMethodCodeNormalizedLanguage($methodCode);
        return $this->orderLineHelperLang[$langCode];
    }
}
