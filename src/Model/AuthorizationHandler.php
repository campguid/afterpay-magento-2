<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AuthorizationHandler
{
    /**
     * @var string
     */
    private $paymentMethodCode;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var int
     */
    private $storeId;

    /**
     * CredentialHandler constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string[]
     */
    public function getConfiguration(): array
    {
        $connectionType = (int) $this->loadConfig('testmode');
        $result['modus'] = $this->getConnectionType($connectionType);
        $result['mode'] = $this->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->loadConfig(sprintf('%s_api_key', $result['modus']));
        return $result;
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    private function loadConfig(string $path)
    {
        $path = sprintf('payment/%s/%s', $this->getPaymentMethodCode(), $path);
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->getStoreId());
    }

    /**
     * @param int $connectionType
     * @param bool $alt
     *
     * @return string
     */
    public function getConnectionType(int $connectionType, bool $alt = false): string
    {
        $connectionTypeMapped = ['production', 'testmode', 'sandbox'];
        if ($alt) {
            $connectionTypeMapped = ['live', 'test', 'sandbox'];
        }
        return $connectionTypeMapped[$connectionType];
    }

    /**
     * @return string
     */
    public function getPaymentMethodCode(): string
    {
        return $this->paymentMethodCode;
    }

    /**
     * @param string $paymentMethodCode
     */
    public function setPaymentMethodCode(string $paymentMethodCode): void
    {
        $this->paymentMethodCode = $paymentMethodCode;
    }

    /**
     * @return int|null
     */
    public function getStoreId(): ?int
    {
        return $this->storeId;
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId): void
    {
        $this->storeId = $storeId;
    }
}
