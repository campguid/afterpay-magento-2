<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Source;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\TestFramework\Event\Magento;

class ProductGroupId implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    protected $attributeFactory;

    /**
     * ProductGroupId constructor.
     *
     * @param CollectionFactory $attributeFactory
     */
    public function __construct(CollectionFactory $attributeFactory)
    {
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * @return array|void
     */
    public function toOptionArray()
    {
        $attributes[] = [
            'value' => '',
            'label' => 'No, do not use'
        ];
        $collection = $this->attributeFactory->create();
        /** @var Attribute $attribute */
        foreach ($collection as $attribute) {
            $attributes[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => sprintf('%s - %s', $attribute->getAttributeCode(), $attribute->getDefaultFrontendLabel())
            ];
        }
        return $attributes;
    }
}
