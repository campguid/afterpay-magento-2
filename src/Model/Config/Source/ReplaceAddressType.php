<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Source;

use \Magento\Framework\Data\OptionSourceInterface;

class ReplaceAddressType implements OptionSourceInterface
{
    const REPLACE_ADDRESS_TYPE_NOTHING_CODE = 0;
    const REPLACE_ADDRESS_TYPE_BILLING_CODE = 1;
    const REPLACE_ADDRESS_TYPE_SHIPPING_CODE = 2;
    const REPLACE_ADDRESS_TYPE_BOTH_CODE = 3;

    /**
     * Connection mode options
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::REPLACE_ADDRESS_TYPE_NOTHING_CODE,
                'label' => __('Do nothing with the address data')
            ],
            [
                'value' => self::REPLACE_ADDRESS_TYPE_BILLING_CODE,
                'label' => __('Overwrite the billing address')
            ],
            [
                'value' => self::REPLACE_ADDRESS_TYPE_SHIPPING_CODE,
                'label' => __('Overwrite the shipping address')
            ],
            [
                'value' => self::REPLACE_ADDRESS_TYPE_BOTH_CODE,
                'label' => __('Overwrite both addresses')
            ]
        ];
    }
}
