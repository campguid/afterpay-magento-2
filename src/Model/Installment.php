<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Payment\Api\InstallmentInterface;
use Afterpay\Afterpay;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;

class Installment implements InstallmentInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var Afterpay
     */
    protected $afterpay;

    /**
     * @var CheckoutSession
     */
    protected $session;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * Ajax constructor.
     *
     * @param Afterpay             $afterpay
     * @param CheckoutSession      $session
     * @param Currency             $currency
     * @param Data                 $helper
     * @param AuthorizationHandler $authorizationHandler
     */
    public function __construct(
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper,
        AuthorizationHandler $authorizationHandler
    ) {
        $this->helper = $helper;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->authorizationHandler = $authorizationHandler;
    }

    /**
     * @param string $paymentMethod
     *
     * @return array|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function lookup($paymentMethod)
    {
        if (\in_array($paymentMethod, Data::$allowedInstalment, true)) {
            $quote = $this->session->getQuote();
            $this->authorizationHandler->setPaymentMethodCode($paymentMethod);
            $this->authorizationHandler->setStoreId($quote->getStoreId());
            $auth = $this->authorizationHandler->getConfiguration();
            $requestData = [
                'amount' => round($quote->getGrandTotal(), 2)
            ];
            $this->afterpay->setRest();
            $this->afterpay->set_ordermanagement('installmentplans_lookup');
            $this->afterpay->set_order($requestData, 'OM');
            $this->afterpay->do_request(
                $auth,
                $auth['mode'],
                $this->helper->getCurrentLocaleNormalized()
            );
            return $this->parseResponse($this->afterpay->order_result->return);
        }
    }

    /**
     * @param \stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response): array
    {
        $result = [];
        $resultResponse = [];
        if (property_exists($response, 'availableInstallmentPlans')) {
            foreach ($response->availableInstallmentPlans as $installmentPlan) {
                $result['value'] = $installmentPlan->installmentProfileNumber;
                $result['totalAmount'] = $this->currency->format($installmentPlan->totalAmount, null, false);
                $result['installmentAmount'] = $this->currency->format(
                    $installmentPlan->installmentAmount,
                    null,
                    false
                );
                $result['startupFee'] = $this->currency->format($installmentPlan->startupFee, null, false);
                $result['monthlyFee'] = $this->currency->format($installmentPlan->monthlyFee, null, false);
                $result['basketAmount'] = $installmentPlan->basketAmount;
                $result['totalAmount_nonformatted'] = $installmentPlan->totalAmount;
                $result['differenceAmount'] = $installmentPlan->totalAmount - $installmentPlan->basketAmount;
                $result['readMore'] = $installmentPlan->readMore;
                $result['effectiveAnnualPercentageRate'] = $installmentPlan->effectiveAnnualPercentageRate;
                $result['numberOfInstallments'] = $installmentPlan->numberOfInstallments;
                $result['effectiveInterestRate'] = $installmentPlan->effectiveInterestRate;
                $result['interestRate'] = $installmentPlan->interestRate;
                $result['optionText'] = sprintf(
                    __(
                        '%s per month in %s installments'
                    ),
                    $result['installmentAmount'],
                    $result['numberOfInstallments']
                );
                $resultResponse[] = $result;
            }
            return $resultResponse;
        }
        $resultResponse[] = ['error' => true];
        return $resultResponse;
    }
}
