<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Api\PaymentMethodCampaignInterface;
use Afterpay\Payment\Helper\Service\Data;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use stdClass;
use function in_array;

class PaymentMethodCampaign implements PaymentMethodCampaignInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Afterpay
     */
    protected $afterpay;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var CheckoutSession
     */
    protected $session;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var int
     */
    protected $storeId;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * @var string
     */
    private $methodCode;

    /**
     * Ajax constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Afterpay             $afterpay
     * @param CheckoutSession      $session
     * @param Currency             $currency
     * @param Data                 $helper
     * @param Session              $customerSession
     * @param AuthorizationHandler $authorizationHandler
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper,
        Session $customerSession,
        AuthorizationHandler $authorizationHandler
    ) {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->customerSession = $customerSession;
        $this->authorizationHandler = $authorizationHandler;
    }

    /**
     * @param string $paymentMethod
     *
     * @return array|string
     * @throws GuzzleException
     */
    public function lookup($paymentMethod)
    {
        $this->setMethodCode($paymentMethod);
        if (in_array($this->getMethodCode(), Data::$allowedCampaigns, true)) {
            $quote = $this->session->getQuote();
            $this->authorizationHandler->setPaymentMethodCode($paymentMethod);
            $this->authorizationHandler->setStoreId($quote->getStoreId());
            $auth = $this->authorizationHandler->getConfiguration();
            $requestData = [
                'order' => [
                    'totalGrossAmount' => round($quote->getGrandTotal(), 2),
                    'totalNetAmount' => round($quote->getSubtotal(), 2)
                ]
            ];
            $this->afterpay->setRest();
            $this->afterpay->set_ordermanagement('available_payment_methods');
            $this->afterpay->set_order($requestData, 'OM');
            $this->afterpay->do_request(
                $auth,
                $auth['mode'],
                $this->helper->getCurrentLocaleNormalized()
            );
            return $this->parseResponse($this->afterpay->order_result->return);
        }
    }

    /**
     * @param stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response): array
    {
        $result = [];
        $position = $this->getCampaignPosition();
        $currentPosition = 1;
        if (property_exists($response, 'paymentMethods')) {
            foreach ($response->paymentMethods as $paymentMethod) {
                if (\in_array($this->getMethodCode(), Data::$allowedCampaigns, true)) {
                    if ($paymentMethod->type === 'Invoice' && property_exists($paymentMethod, 'campaigns')) {
                        if ($currentPosition === $position) {
                            $campaign = $paymentMethod->campaigns;
                            $result[] = [
                                'campaignNumber' => $campaign->campaignNumber,
                                'title' => $paymentMethod->title,
                                'description' => $paymentMethod->tag,
                                'consumerFeeAmount' => $this->currency->format(
                                    $campaign->consumerFeeAmount,
                                    null,
                                    false
                                ),
                            ];
                            break;
                        }
                        $currentPosition++;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Gets position to which use from the response, as the response can contain multiple campaign invoices
     *
     * @return int|null
     */
    private function getCampaignPosition(): int
    {
        $positionMapping = [
            'campaign' => 1,
            'campaign2' => 2,
            'campaign3' => 3,
        ];
        $splitMethodCode = explode('_', $this->getMethodCode());
        $normalizedString = end($splitMethodCode);
        return $positionMapping[$normalizedString] ?? 0;
    }

    /**
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * @param string $methodCode
     */
    public function setMethodCode(string $methodCode): void
    {
        $this->methodCode = $methodCode;
    }
}
