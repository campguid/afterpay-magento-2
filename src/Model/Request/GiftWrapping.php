<?php

/**
 * @category  Scandiweb
 * @package   ${NAMESPACE}
 * @author    austris <info@scandiweb.com>
 * @copyright Copyright (c) 2021 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model\Request;

use Amasty\Checkout\Model\FeeRepository;
use Magento\Framework\App\ObjectManager;

class GiftWrapping
{
    /**
     * @var FeeRepository|null
     */
    private $feeRepository;

    public function __construct()
    {
        $this->getFeeRepository();
    }

    /**
     * If repository is available and fee exists, return value
     *
     * @param int $quoteId
     *
     * @return float|null
     */
    public function getWrappingPrice(int $quoteId): ?float
    {
        if ($this->feeRepository === null) {
            return null;
        }

        $fee = $this->feeRepository->getByQuoteId($quoteId);
        if (!$fee) {
            return null;
        }

        return (float) $fee->getAmount();
    }

    /**
     * We cannot rely on Amasty module being installed and FeeRepositoryInterface class available
     *  so it can't be injected through constructor. Use objectManager instead.
     */
    private function getFeeRepository(): void
    {
        $objectManager = ObjectManager::getInstance();

        if (class_exists(FeeRepository::class)) {
            $this->feeRepository = $objectManager->get(FeeRepository::class);
        }
    }
}
