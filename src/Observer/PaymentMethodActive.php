<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Afterpay;
use Afterpay\Payment\Model\Config\Visitor;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\Serializer\Serialize;
use stdClass;

class PaymentMethodActive implements ObserverInterface
{
    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var Visitor
     */
    private $visitor;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var Serialize
     */
    private $serializer;

    /**
     * @param Visitor        $visitor
     * @param Data           $helper
     * @param Afterpay       $afterpay
     * @param Session        $session
     * @param Currency       $currency
     * @param CacheInterface $cache
     * @param Serialize      $serializer
     */
    public function __construct(
        Visitor $visitor,
        Data $helper,
        Afterpay $afterpay,
        Session $session,
        Currency $currency,
        CacheInterface $cache,
        Serialize $serializer
    ) {
        $this->visitor = $visitor;
        $this->helper = $helper;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->cache = $cache;
        $this->serializer = $serializer;
    }

    /**
     * @param Observer $observer
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        /* @var MethodInterface $methodInstance */
        $methodInstance = $event->getData('method_instance');
        /* @var Quote $quote */
        $quote = $event->getData('quote');
        $result = $event->getData('result');

        if ($this->shouldSkip($quote, $result, $methodInstance)) {
            return;
        }

        $available = $this->allowedForGroup($methodInstance, $quote)
            && $this->allowedIp($methodInstance)
            && $this->allowedShippingMethod($methodInstance, $quote)
            && $this->allowedAccountMethod($methodInstance, $quote);
        $result->setData('is_available', $available);
    }

    /**
     * @param Quote|null $quote
     * @param DataObject $result
     * @param MethodInterface $methodInstance
     *
     * @return bool
     */
    private function shouldSkip($quote, DataObject $result, MethodInterface $methodInstance): bool
    {
        return $quote === null
            || $result->getData('is_available') === false
            || strpos($methodInstance->getCode(), 'afterpay') !== 0; // does not start with
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     */
    private function allowedForGroup(MethodInterface $methodInstance, Quote $quote): bool
    {
        if ($methodInstance->getConfigData('allowspecificgroup')) {
            $allowedGroups = explode(',', $methodInstance->getConfigData('specificgroup'));

            return in_array((string)$quote->getCustomerGroupId(), $allowedGroups, true);
        }

        return true;
    }

    /**
     * @param MethodInterface $methodInstance
     *
     * @return bool
     */
    private function allowedIp(MethodInterface $methodInstance): bool
    {
        if ($methodInstance->getConfigData('restrict')) {
            return $this->visitor->allowedByIp();
        }

        return true;
    }

    /**
     * Is allowed payment for shipping method or not
     *
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     */
    private function allowedShippingMethod(MethodInterface $methodInstance, Quote $quote): bool
    {
        $config = $methodInstance->getConfigData('excludeships');
        if ($config === null) {
            return true;
        }
        $methods = explode(',', $config);

        return !in_array($quote->getShippingAddress()->getShippingMethod(), $methods, true);
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function allowedAccountMethod(MethodInterface $methodInstance, Quote $quote): bool
    {
        if (\in_array($methodInstance->getCode(), Data::$allowedMethods, true)) {
            $auth = $this->helper->getConfiguration($methodInstance->getCode());
            $cacheKey = sprintf('%s_%s_%s', $auth['apiKey'], $quote->getGrandTotal(), $quote->getStoreId());
            $cacheLifetime = 5;
            $cache = $this->cache->load($cacheKey);
            if ($cache) {
                $data = $this->serializer->unserialize($cache);
                $data = json_decode(json_encode($data), false);
            } else {
                $data = $this->getResponseData($auth, $quote);
                $serializedData = $this->serializer->serialize($data);
                $this->cache->save($serializedData, $cacheKey, [], $cacheLifetime);
            }
            return $this->parseResponse($data, $methodInstance->getCode());
        }

        return true;
    }

    /**
     * @param stdClass $response
     * @param string $methodCode
     *
     * @return bool
     */
    private function parseResponse(stdClass $response, string $methodCode): bool
    {
        $campaignMethods = [];
        if (property_exists($response, 'paymentMethods')) {
            foreach ($response->paymentMethods as $paymentMethod) {
                if (\in_array($methodCode, Data::$allowedFlex, true)
                ) {
                    if ($paymentMethod->type === 'Account' && property_exists($paymentMethod, 'account')) {
                        $profileNo = $paymentMethod->account->profileNo;
                        $this->session->setAccountProfileNo($profileNo);
                        return true;
                    }
                }
                if (\in_array($methodCode, Data::$allowedInstalment, true)
                ) {
                    if ($paymentMethod->type === 'Installment' && property_exists($paymentMethod, 'installment')) {
                        return true;
                    }
                }
            }
            if (\in_array($methodCode, Data::$allowedCampaigns, true)) {
                foreach ($response->paymentMethods as $paymentMethod) {
                    if ($paymentMethod->type === 'Invoice' && property_exists($paymentMethod, 'campaigns')) {
                        $campaignMethods[] = $paymentMethod;
                    }
                }
                $campaignPosition = $this->getCampaignPosition($methodCode);
                if ($campaignMethods
                    && $campaignPosition !== null
                    && array_key_exists($campaignPosition, $campaignMethods)
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param array $auth
     * @param Quote $quote
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getResponseData(array $auth, Quote $quote)
    {
        $requestData = [
            'order' => [
                'totalGrossAmount' => round($quote->getGrandTotal(), 2),
                'totalNetAmount' => round($quote->getSubtotal(), 2)
            ]
        ];
        $this->afterpay->setRest();
        $this->afterpay->set_ordermanagement('available_payment_methods');
        $this->afterpay->set_order($requestData, 'OM');
        $this->afterpay->do_request($auth, $auth['mode'], $this->helper->getCurrentLocaleNormalized());
        return $this->afterpay->order_result->return;
    }

    /**
     * Gets position to which use from the response, as the response can contain multiple campaign invoices
     *
     * @param string $methodCode
     *
     * @return int|null
     */
    private function getCampaignPosition(string $methodCode): ?int
    {
        $positionMapping = [
            'campaign' => 0,
            'campaign2' => 1,
            'campaign3' => 2,
        ];
        $splitMethodCode = explode('_', $methodCode);
        $normalizedString = end($splitMethodCode);
        return $positionMapping[$normalizedString] ?? null;
    }
}
