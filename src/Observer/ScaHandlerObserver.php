<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Model\ScaHandler;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random;
use Magento\Framework\Url\Encoder;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;

class ScaHandlerObserver implements ObserverInterface
{
    public const MERCHANT_URL = 'afterpay/payment/sca';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var Random
     */
    private $mathRandom;
    /**
     * @var Encoder
     */
    private $encoder;

    /**
     * @param UrlInterface    $urlBuilder
     * @param OrderRepository $orderRepository
     * @param Random          $mathRandom
     * @param Encoder         $encoder
     */
    public function __construct(
        UrlInterface $urlBuilder,
        OrderRepository $orderRepository,
        Random $mathRandom,
        Encoder $encoder
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->orderRepository = $orderRepository;
        $this->mathRandom = $mathRandom;
        $this->encoder = $encoder;
    }

    public function execute(Observer $observer): void
    {
        $events = $observer->getEvent();
        $order = $this->getOrder($events);
        if (!isset($order)) {
            return;
        }
        $this->updateOrder($order);
    }

    /**
     * @param string $hash
     *
     * @return string
     */
    protected function getMerchantUrl(string $hash): string
    {
        return $this->urlBuilder->getUrl(self::MERCHANT_URL, ['token' => $hash]);
    }

    /**
     * @param Order  $order
     * @param string $hash
     *
     * @return string
     */
    protected function createRedirectUrl(Order $order, string $hash): ?string
    {
        $secureUrl = $order->getAfterpayScaUrl();
        if (!$secureUrl) {
            return null;
        }
        $merchantUrl = $this->getMerchantUrl($hash);
        return sprintf('%s%s', $secureUrl, urlencode($merchantUrl));
    }

    /**
     * @param Event $event
     *
     * @return mixed
     */
    protected function getOrder(Event $event)
    {
        return $event->getData('order');
    }

    /**
     * @param $order
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function updateOrder(Order $order): void
    {
        $hash = $this->mathRandom->getUniqueHash();
        $redirectUrl = $this->createRedirectUrl($order, $hash);
        if (!$redirectUrl) {
            return;
        }
        $order->setStatus(ScaHandler::ORDER_STATUS_SCA_PENDING_CODE)->setState(Order::STATE_PAYMENT_REVIEW);
        $order->setAfterpayScaRedirectUrl($this->encoder->encode($redirectUrl));
        $order->setAfterpayScaHash($hash);
        $order->addCommentToStatusHistory(
            __('Strong Customer Authentication has been requested from the customer by Afterpay')
        );
        try {
            $this->orderRepository->save($order);
        } catch (AlreadyExistsException | InputException | NoSuchEntityException $e) {
            return;
        }
    }
}
