<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Setup;

use Afterpay\Payment\Model\ScaHandler;
use Exception;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\Order;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var EavSetup $eavSetup
     */
    private $eavSetup;

    /**
     * @var StatusResourceFactory
     */
    private $statusResourceFactory;

    /**
     * @var StatusFactory
     */
    private $statusFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param CustomerSetupFactory $customerSetupFactory
     * @param StatusResourceFactory $resourceStatusFactory
     * @param StatusFactory $statusFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        StatusResourceFactory $statusResourceFactory,
        StatusFactory $statusFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->statusResourceFactory = $statusResourceFactory;
        $this->statusFactory = $statusFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     * @throws LocalizedException
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.7.1', '<')) {
            $this->addCocAttributeToQuote($setup);
        }

        if (version_compare($context->getVersion(), '3.2.0', '<')) {
            $this->addNewOrderStatuses();
        }

        if (version_compare($context->getVersion(), '3.4.9', '<')) {
            $this->removeGenderFields($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @throws LocalizedException
     */
    private function addCocAttributeToQuote(ModuleDataSetupInterface $setup)
    {
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (!$this->eavSetup->getAttribute(Customer::ENTITY, 'cocnumber')) {
            $this->eavSetup->addAttribute(Customer::ENTITY, 'cocnumber', [
                'type' => 'static',
                'input' => 'text',
                'label' => 'CoC number',
                'required' => false,
                'system' => 0,
            ]);
        }

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $cocnumberAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'cocnumber');
        $cocnumberAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $cocnumberAttribute->save();
    }

    private function addNewOrderStatuses(): void
    {
        $statuses = [
            [
                'label' => ScaHandler::ORDER_STATUS_SCA_PENDING_LABEL,
                'code' => ScaHandler::ORDER_STATUS_SCA_PENDING_CODE
            ],
            [
                'label' => ScaHandler::ORDER_STATUS_SCA_FAILED_LABEL,
                'code' => ScaHandler::ORDER_STATUS_SCA_FAILED_CODE
            ]
        ];

        foreach ($statuses as $statusArray) {
            /** @var StatusResource $statusResource */
            $statusResource = $this->statusResourceFactory->create();
            $status = $this->statusFactory->create();
            $status->setData(
                [
                    'status' => $statusArray['code'],
                    'label' => $statusArray['label'],
                ]
            );
            try {
                $statusResource->save($status);
                $status->assignState(Order::STATE_PAYMENT_REVIEW, false, false);
            } catch (AlreadyExistsException | Exception $exception) {
                return;
            }
        }
    }

    /**
     * Remove config setting for gender field where it isn't necessary
     * 
     * @param $setup
     */
    private function removeGenderFields($setup): void
    {
        $configItems = [
            'payment/afterpay_at_direct_debit/gender',
            'payment/afterpay_at_installment/gender',
            'payment/afterpay_at_open_invoice/gender',
            'payment/afterpay_de_b2b/gender',
            'payment/afterpay_de_direct_debit/gender',
            'payment/afterpay_de_installment/gender',
            'payment/afterpay_de_invoice/gender',
            'payment/afterpay_nl_rest_b2b/gender',
            'payment/afterpay_nl_rest_direct_debit/gender',
            'payment/afterpay_nl_rest_invoice_extra/gender',
            'payment/afterpay_nl_rest_invoice/gender',
            'payment/afterpay_ch_open_invoice/gender'
        ];

        $setup->getConnection()->delete(
            $setup->getTable('core_config_data'),
            "path IN ('" . implode("','", $configItems) . "')"
        );
    }
}
