/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery'
], function ($) {
    'use strict';

    let errorMessages = [
        'Please make sure the date is correct.',
        'To make use of the AfterPay payment method, your age has to be 18 years or older.',
    ];
    let errorKey = 0;

    return function () {
        $.validator.addMethod(
            'customer-dob-validator',
            function (value) {
                let formattedDate = value.split('/'); // DAY/MONTH/YEAR, e.g. 21/02/2022
                let noError = true;
                let isYounger = false;

                errorKey = 0;

                $(formattedDate).each(function (index) {
                    switch (index) {
                        case 0:
                            if (parseFloat(formattedDate[index]) === 0 || parseFloat(formattedDate[index]) > 31) {
                                noError = false;
                            }
                            break;
                        case 1:
                            if (parseFloat(formattedDate[index]) === 0 || parseFloat(formattedDate[index]) > 12) {
                                noError = false;
                            }
                            break;
                        case 2:
                            let setDate = new Date(
                                parseFloat(formattedDate[index]) + 18,
                                parseFloat(formattedDate[1]) - 1,
                                parseFloat(formattedDate[0])
                            );
                            let currentDate = new Date();

                            if (formattedDate[index].length !== 4) {
                                noError = false;
                            }

                            // if under 18 years old
                            if (currentDate < setDate) {
                                isYounger = true;
                                noError = false;
                                errorKey = 1;
                            }
                            break;
                        default:
                            break;
                    }

                    if (!noError || isYounger) {
                        return false;
                    }
                });
                return noError;
            },
            function () {
                return $.mage.__(errorMessages[errorKey]);
            }
        )
    }
});
