/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'Magento_Checkout/js/checkout-data',
        'mage/translate',
        'Magento_Checkout/js/model/quote',
        'uiLayout',
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader',
        'inputmask'
    ],
    function (
        ko,
        Component,
        $,
        checkoutData,
        $t,
        quote,
        layout,
        urlBuilder,
        loader,
        inputmask
    ) {
        'use strict';

        return Component.extend({
            afterpayConfig: '',
            defaults: {
                template: 'Afterpay_Payment/payment/payment',
                customerGender: 2, // Female,
                dobFull: '',
                termsAndConditions: false,
                telNumber: '',
                cocNumber: '',
                companyName: '',
                bankaccountNumber: '',
                ssn: '',
                methodCode: '',
                redirectAfterPlaceOrder: false,
                termsLinkBase: 'https://documents.myafterpay.com/consumer-terms-conditions/',
                privacyLinkBase: 'https://documents.myafterpay.com/privacy-statement/',
                termsLocale: 'en_us',
            },

            initialize: function () {
                var self = this;
                self._super();
                self.afterpayConfig = self.getAfterpayConfig();
                self.customerGender = ko.observable(this.getCustomerGender());
                self.dobFull = ko.observable(this.getFullDob());
                self.termsAndConditions = ko.observable();
                self.telNumber = ko.observable(this.getTelNumber());
                self.cocNumber = ko.observable(this.getCocNumber());
                self.companyName = ko.observable(this.getCompanyName());
                self.bankaccountNumber = ko.observable(this.getBankaccountNumber());
                self.ssn = ko.observable(this.getSsn());
                self.installment = ko.observable();
                self.defaultGender = 2;
                self.config = {
                    'flex_url': 'rest/V1/afterpay/flex/lookup',
                    'redirect_url': 'afterpay/payment/redirect',
                };
                self.flexData = [];
                self.flexInfo = ko.observableArray(self.flexData);
                self.methodName = '';
                self.availableMethods = {
                    'invoice': 'invoice',
                    'debit': 'direct_debit',
                    'installment': 'fix_installments',
                    'campaign': 'campaign_invoice',
                    'flex': 'part_payment'
                };

                // Adding date mask to dob field in checkout page
                $('input.dob').mask('00/00/0000', {placeholder: $t('DD/MM/YYYY')});
            },

            getCode: function () {
                return this.methodCode;
            },

            isActive: function () {
                return true;
            },

            isAvailable: function () {
                return true;
            },

            getTitle: function () {
                return this.afterpayConfig.title;
            },

            getDescription: function () {
                return this.afterpayConfig.description;
            },

            isPlaceOrderActionAllowedAfterpay: function () {
                return this.validateCountries();
            },

            validateCountries: function () {
                var billingAddress = false;
                if (checkoutData.getSelectedBillingAddress() === null) {
                    if (checkoutData.getShippingAddressFromData() === null && window.checkoutConfig.customerData.hasOwnProperty('addresses')) {
                        // 'Use same address' has been checked and user hasn't explicitly selected any shipping method,
                        // falling back to default shipping address.
                        $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                            if (address.default_billing) {
                                billingAddress = address;
                            }
                        }.bind(this));
                    } else {
                        // 'Use same address' has been checked and user has entered shipping info
                        billingAddress = checkoutData.getShippingAddressFromData();
                    }
                } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-address') {
                    // New billing address entered
                    billingAddress = checkoutData.getNewCustomerBillingAddress();
                } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-billing-address') {
                    // New billing address entered
                    billingAddress = checkoutData.getNewCustomerBillingAddress();
                } else {
                    // Predefined address selected as custom billing address
                    var selectedAddressId = parseInt(checkoutData.getSelectedBillingAddress().substring('customer-address'.length));
                    $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                        if (parseInt(address.id) === selectedAddressId) {
                            billingAddress = address;
                        }
                    }.bind(this));
                }
                return this.validateCountryCode(billingAddress);
            },

            validateCountryCode: function (address) {
                if (this.afterpayConfig && address) {
                    if (this.afterpayConfig.allowspecific === "0") {
                        return true
                    }
                    var allowedCodes = this.afterpayConfig.allowed_countries.split(',');
                    return (allowedCodes.indexOf(address.country_id) >= 0);
                }
                return false;
            },

            getAfterpayConfig: function () {
                if (window.checkoutConfig.payment) {
                    var afterpayConfig = window.checkoutConfig.payment[this.methodCode];
                    if (afterpayConfig) {
                        return afterpayConfig;
                    }
                }
                return null;
            },

            getTestmodeLabel: function () {
                if (this.isTestmode()) {
                    return this.afterpayConfig.testmode_label;
                }
            },

            getPaymentIcon: function () {
                return this.afterpayConfig.payment_icon;
            },

            isTestmode: function () {
                // Force it to return boolean
                return !!parseInt(this.afterpayConfig.testmode);
            },

            getCustomerGender: function () {
                if (window.customerData && window.customerData.gender) {
                    return window.customerData.gender;
                }
                return this.defaultGender;

            },

            getTelNumber: function () {
                return quote.billingAddress() ? quote.billingAddress().telephone : null;
            },

            getFullDob: function () {
                return window.customerData.dob;
            },

            getGenderValues: function () {
                return [
                    {'optionText': $t('Female'), optionValue: 2},
                    {'optionText': $t('Male'), optionValue: 1}
                ]
            },

            getData: function () {
                var self = this,
                    result = {
                        'method': self.item.method,
                        'po_number': null,
                        'additional_data': null
                    };

                // Add customer gender
                if (self.canShowGender && self.customerGender()) {
                    result.additional_data = {
                        customer_gender: self.customerGender()
                    };
                } else if (!self.canShowGender() && self.afterpayConfig.gender_fallback) {
                    // Load value by name and select firt one
                    var genderValue = $("[name='" + self.afterpayConfig.gender_fallback + "']:first");
                    result.additional_data = {
                        customer_gender: genderValue.val()
                    };
                }

                // Add customer dob
                if (self.canShowDob() && self.getDob()) {
                    if (result.additional_data) {
                        result.additional_data.customer_dob = self.getDob();
                    } else {
                        result.additional_data = {
                            customer_dob: self.getDob()
                        }
                    }
                } else if (!self.canShowDob() && self.afterpayConfig.dob_fallback) {
                    // Load value by name and select firt one
                    var dobValue = $("[name='" + self.afterpayConfig.dob_fallback + "']:first");
                    if (result.additional_data) {
                        result.additional_data.customer_dob = dobValue.val();
                    } else {
                        result.additional_data = {
                            customer_dob: dobValue.val()
                        }
                    }
                }

                // Add terms and conditions
                if (self.canShowConditions()) {
                    if (result.additional_data) {
                        result.additional_data.terms_and_conditions = self.termsAndConditions();
                    } else {
                        result.additional_data = {
                            terms_and_conditions: self.termsAndConditions()
                        }
                    }
                }

                // Add tel number
                if (self.canShowNumber() && self.telNumber()) {
                    if (result.additional_data) {
                        result.additional_data.customer_telephone = self.telNumber();
                    } else {
                        result.additional_data = {
                            customer_telephone: self.telNumber()
                        }
                    }
                } else if (!self.canShowNumber() && self.afterpayConfig.phone_fallback) {
                    // Load value by name and select firt one
                    var phoneValue = $("[name='" + self.afterpayConfig.phone_fallback + "']:first");
                    if (result.additional_data) {
                        result.additional_data.customer_telephone = phoneValue.val();
                    } else {
                        result.additional_data = {
                            customer_telephone: phoneValue.val()
                        }
                    }
                }

                // Add social security number
                if (self.canShowSsn()) {
                    if (result.additional_data) {
                        result.additional_data.ssn = self.ssn();
                    } else {
                        result.additional_data = {
                            ssn: self.ssn()
                        }
                    }
                }

                // Add coc number
                if (self.cocNumber()) {
                    if (result.additional_data) {
                        result.additional_data.coc_number = self.cocNumber();
                    } else {
                        result.additional_data = {
                            coc_number: self.cocNumber()
                        }
                    }
                }

                // Add company name
                if (self.companyName()) {
                    if (result.additional_data) {
                        result.additional_data.company_name = self.companyName();
                    } else {
                        result.additional_data = {
                            company_name: self.companyName()
                        }
                    }
                }

                //Add bank account number
                if (self.bankaccountNumber()) {
                    if (result.additional_data) {
                        result.additional_data.bankaccountnumber = self.bankaccountNumber()
                    } else {
                        result.additional_data = {
                            bankaccountnumber: self.bankaccountNumber()
                        }
                    }
                }

                //Add installment ID
                if (self.installment()) {
                    if (result.additional_data) {
                        result.additional_data.installment = self.installment().value;
                    } else {
                        result.additional_data = {
                            installment: self.installment().value
                        }
                    }
                }

                if (this.afterpayConfig.tracking_active === '1') {
                    if (result.additional_data) {
                        result.additional_data.tracking_id = this.afterpayConfig.unique_id;
                    } else {
                        result.additional_data = {
                            tracking_id: this.afterpayConfig.unique_id
                        }
                    }
                }

                return result;
            },

            getDobMonths: function () {
                var result = [
                    {'optionText': $t('Month'), 'optionValue': ''}
                ];

                // Populate our months select box
                for (var i = 1; i < 13; i++) {
                    result.push({'optionText': $t(i), 'optionValue': i});
                }

                return result;
            },

            getDobYears: function () {
                var result = [
                    {'optionText': $t('Year'), 'optionValue': ''}
                ];

                // Populate our years select box
                for (var i = this.firstValidDobYear(); i > 1900; i--) {
                    result.push({'optionText': $t(i), 'optionValue': i});
                }

                return result;
            },

            /**
             * Do not show last 18 years in dropdown as person needs to be at least 18 to use Afterpay
             *
             * @returns {number}
             */
            firstValidDobYear: function () {
                return new Date().getFullYear() - 18;
            },

            // Dob helper
            daysInMonth: function (month, year) {
                return new Date(year, month, 0).getDate();
            },

            getDob: function () {
                var self = this,
                    fullDate = self.dobFull();

                return fullDate;
            },


            getCocNumber: function () {
                try {
                    return window.customerData.custom_attributes.cocnumber.value;
                } catch (e) {
                    return null;
                }
            },

            getBankaccountNumber: function () {
                return quote.billingAddress() ? quote.billingAddress().bankaccountNumber : null;
            },

            getCompanyName: function () {
                return quote.billingAddress() ? quote.billingAddress().company : null;
            },

            getSsn: function () {
                return quote.billingAddress() ? quote.billingAddress().ssn : null;
            },

            getMethodName: function () {
                for (let key in this.availableMethods) {
                    if (this.methodCode.indexOf(key) !== -1) {
                        this.methodName = this.availableMethods[key];
                    }
                }

                return this.methodName;
            },

            getPostfix: function() {
                return this.termsLocale + '/' + (this.merchantId || 'default') + '/' + (this.getMethodName() || '');
            },

            getPrivacyStatementPostFix: function() {
                return this.termsLocale + '/' + (this.merchantId || 'default');
            },

            getConditionsLink: function () {
                if (this.termsLocale === 'nl_NL') {
                    return 'https://www.afterpay.nl/nl/algemeen/zakelijke-partners/betalingsvoorwaarden-zakelijk';
                } else {
                    return this.termsLinkBase + this.getPostfix();
                }
            },

            getPrivacystatementLink: function () {
                return this.privacyLinkBase + this.getPrivacyStatementPostFix();
            },

            getSsnNoticeLink: function () {},

            canShowSsnNotice: function () {
                return false;
            },

            canShowConditions: function () {
                return this.afterpayConfig.terms_and_conditions != 0;
            },

            canShowCoc: function () {
                return this.afterpayConfig.can_coc != 0;
            },

            canShowBankaccount: function () {
                return this.afterpayConfig.can_bankaccount != 0;
            },

            canShowDob: function () {
                return this.afterpayConfig.can_dob != 0;
            },

            canShowSsn: function () {
                return this.afterpayConfig.can_ssn != 0;
            },

            canShowCompanyName: function () {
                return this.afterpayConfig.can_company_name != 0;
            },

            canShowPrivacy: function () {
                return this.afterpayConfig.can_privacy === "1";
            },

            canShowGender: function () {
                return this.afterpayConfig.can_gender === "1";
            },

            canShowNumber: function () {
                return this.afterpayConfig.can_number === "1";
            },

            getTrackingPixel: function () {
                if (this.afterpayConfig.tracking_active === '1') {
                    window._itt = {
                        c: this.afterpayConfig.tracking_id,
                        s: this.afterpayConfig.unique_id,
                        t: 'CO'
                    };
                    (function () {
                        var scriptElement = document.createElement('script');
                        scriptElement.type = 'text/javascript';
                        scriptElement.async = true;
                        scriptElement.src = '//uc8.tv/7982.js';

                        var scriptElementTag = document.getElementsByTagName('script')[0];
                        scriptElementTag.parentNode.insertBefore(scriptElement, scriptElementTag);
                    })();
                }
            },

            isTrackingActive: function () {
                return this.afterpayConfig.tracking_id;
            },

            getTrackingSessionId: function () {
                return this.afterpayConfig.unique_id;
            },

            createMessagesComponent: function () {
                var messagesComponent = {
                    parent: this.name,
                    name: this.name + '.messages',
                    displayArea: 'messages',
                    component: 'Afterpay_Payment/js/view/messages',
                    config: {
                        messageContainer: this.messageContainer,
                        messageTimeout: window.checkoutConfig.messageTimeout
                    }
                };
                layout([messagesComponent]);

                return this;
            },

            canShowFlexInfo: function () {
                return this.afterpayConfig.can_show_flex;
            },

            getFlexInfo: function () {
                return  this.flexInfo()[0];
            },

            getFlexDataFromServer: function () {
                loader.startLoader();
                var payload = {
                    paymentMethod: this.methodCode
                };

                $.ajax({
                    url: urlBuilder.build(this.config.flex_url),
                    type: 'POST',
                    data: JSON.stringify(payload),
                    async: false,
                    contentType: 'application/json'
                }).done(
                    function (response) {
                        this.flexData.push(response[0]);
                    }.bind(this)
                ).fail(
                    function (response) {
                    }
                ).always(
                    function () {
                        loader.stopLoader();
                    }
                );
            },

            afterPlaceOrder: function () {
                window.location.replace(urlBuilder.build(this.config.redirect_url))
            }
        });
    }
);
