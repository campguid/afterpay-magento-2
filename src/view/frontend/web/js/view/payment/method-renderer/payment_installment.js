/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

define(
    [
        'Afterpay_Payment/js/view/payment/method-renderer/payment_default',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/storage',
        'jquery',
        'mage/url'
    ],
    function (
        Component,
        ko,
        loader,
        storage,
        $,
        urlBuilder
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Afterpay_Payment/payment/installment-payment',
            },

            initialize: function () {
                var self = this;
                self._super();
                self.config = {
                    ...self.config,
                    'url': 'rest/V1/afterpay/installments/lookup'
                };
                self.methodCode = 'afterpay_de_installment';
                self.methodName = 'fix_installments'
                self.afterpayConfig = self.getAfterpayConfig();
                self.installmentData = [];
                self.infoTemplate = 'Afterpay_Payment/payment/installment-payment-info';
                self.installments = ko.observableArray(self.installmentData);
                self.selectionChange = self.onSelectionChange.bind(self);
                self.installment = ko.observableArray([]);
                self.selectFirst = self.doSelectFirst.bind(self);
                self.isSelected = self.doIsSelected.bind(self);
            },

            getPrivacystatementLink: function () {
                return 'https://documents.myafterpay.com/privacy-statement/de_de';
            },

            getInstallmentPlans: function () {
                loader.startLoader();
                var payload = {
                    paymentMethod: this.methodCode
                }, self = this;

                $.ajax({
                    url: urlBuilder.build(this.config.url),
                    type: 'POST',
                    data: JSON.stringify(payload),
                    async: false,
                    contentType: 'application/json'
                }).done(
                    function (response) {
                        $.each(response, function (index, option) {
                            self.installmentData.push(option);
                        });
                    }.bind(this)
                ).fail(
                    function (response) {
                    }
                ).always(
                    function () {
                        loader.stopLoader();
                    }
                );
            },

            getInstallmentInfoTemplate: function () {
                return this.infoTemplate;
            },

            doSelectFirst(installment) {
                if (parseInt(installment.value) !== 1) {
                    return false;
                }
                this.installment(installment);

                return true;
            },

            onSelectionChange: function (selection) {
                this.installment(selection);

                return true;
            },

            hasStaticText: function () {
                return this.afterpayConfig.installment_static_text.length > 1;
            },

            getStaticText: function () {
                return this.afterpayConfig.installment_static_text;
            },

            doIsSelected: function (installment) {
                return installment.value === this.installment().value;
            }
        });
    }
);
